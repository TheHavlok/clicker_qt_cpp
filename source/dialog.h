#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTimer>
#include "ui_mainwindow.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

signals:
    void score(QString text);
    void automatic(QString text);
    void price1(QString text);
    void price2(QString text);
    void price3(QString text);
    void price4(QString text);
    void price5(QString text);
    void price6(QString text);

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

    void setText(const QString& text);
    void autom(const QString& text);

    void setPrice1(const QString& text);
    void setPrice2(const QString& text);
    void setPrice3(const QString& text);
    void setPrice4(const QString& text);
    void setPrice5(const QString& text);
    void setPrice6(const QString& text);

private slots:
    void on_buy1_clicked();
    void on_close_clicked();
    void on_buy2_clicked();
    void on_buy3_clicked();
    void on_buy4_clicked();
    void on_buy5_clicked();
    void on_buy6_clicked();

    void setautomatic(); // для таймера

private:
    Ui::Dialog *ui;

    QTimer a_timer;
};

#endif // DIALOG_H
