#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include "ui_dialog.h"
#include <QFile>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    setWindowFlags(Qt::Dialog |
                   Qt::MSWindowsFixedSizeDialogHint |
                   Qt::WindowCloseButtonHint);

    ui->setupUi(this);

    // таймер для автоматическго обновления
    connect(&m_timer, SIGNAL(timeout()), SLOT(setauto()));
    m_timer.start(1000);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// автоматическое обнавление
void MainWindow::setauto()
{
    double autom, score;
    autom = ui->automatic->text().toDouble();
    score = ui->score->text().toDouble();
    score = score + autom;
    ui->score->setText(QString::number(score, 'f', 3));
}

void MainWindow::on_mine_clicked()
{
    double tap1 = 0.001, tap; // по сколько прибавиться за клик
    tap = ui->score->text().toDouble();
    tap = tap + tap1;
    ui->score->setText(QString::number(tap, 'f', 3)); // выводим до 3 знака после запятой
}

void MainWindow::on_shop_clicked()
{
    Dialog window;
    window.setModal(true);
    window.setText(this->ui->score->text()); // отправляем значение "score"
    window.autom(this->ui->automatic->text()); // отправляем значение "automatic"
    window.setPrice1(this->ui->pr1->text()); // отправляем значение "pr1"
    window.setPrice2(this->ui->pr2->text()); // отправляем значение "pr2"
    window.setPrice3(this->ui->pr3->text()); // отправляем значение "pr3"
    window.setPrice4(this->ui->pr4->text()); // отправляем значение "pr4"
    window.setPrice5(this->ui->pr5->text()); // отправляем значение "pr5"
    window.setPrice6(this->ui->pr6->text()); // отправляем значение "pr6"

// принимаем значения что бы они не пропали по закрытию второго окна "Dialog"
    // принимаем значение score
    connect(&window, &Dialog::score, [&](QString text)
    {
        ui->score->setText(text);
    });

    // принимаем значение automatic
    connect(&window, &Dialog::automatic, [&](QString text)
    {
       ui->automatic->setText(text);
    });

    // принимаем значение price1
    connect(&window, &Dialog::price1, [&](QString text)
    {
        ui->pr1->setText(text);
    });

    // принимаем значение price2
    connect(&window, &Dialog::price2, [&](QString text)
    {
        ui->pr2->setText(text);
    });

    // принимаем значение price3
    connect(&window, &Dialog::price3, [&](QString text)
    {
        ui->pr3->setText(text);
    });

    // принимаем значение price4
    connect(&window, &Dialog::price4, [&](QString text)
    {
        ui->pr4->setText(text);
    });

    // принимаем значение price5
    connect(&window, &Dialog::price5, [&](QString text)
    {
        ui->pr5->setText(text);
    });

    // принимаем значение price6
    connect(&window, &Dialog::price6, [&](QString text)
    {
        ui->pr6->setText(text);
    });

    window.exec();
}

// сохраняем прогресс
void MainWindow::on_savePush_clicked()
{
// присваиваем переменным значения из ui
    double sco, aut, pr1, pr2, pr3, pr4, pr5, pr6;
    sco = ui->score->text().toDouble();
    aut = ui->automatic->text().toDouble();
    pr1 = ui->pr1->text().toDouble();
    pr2 = ui->pr2->text().toDouble();
    pr3 = ui->pr3->text().toDouble();
    pr4 = ui->pr4->text().toDouble();
    pr5 = ui->pr5->text().toDouble();
    pr6 = ui->pr6->text().toDouble();
// удаляем файлы с сохранением за случай если они уже были
    QFile("saves/score.txt").remove();
    QFile("saves/auto.txt").remove();
    QFile("saves/pr1.txt").remove();
    QFile("saves/pr2.txt").remove();
    QFile("saves/pr3.txt").remove();
    QFile("saves/pr4.txt").remove();
    QFile("saves/pr5.txt").remove();
    QFile("saves/pr6.txt").remove();
// создём файлы куда будем сохранять прогресс
    QFile savescore("saves/score.txt"), saveauto("saves/auto.txt"), savepr1("saves/pr1.txt")
            , savepr2("saves/pr2.txt"), savepr3("saves/pr3.txt"), savepr4("saves/pr4.txt")
            , savepr5("saves/pr5.txt"), savepr6("saves/pr6.txt");

    if (savescore.open(QIODevice::Append) && saveauto.open(QIODevice::Append) &&
            savepr1.open(QIODevice::Append) && savepr2.open(QIODevice::Append) &&
            savepr3.open(QIODevice::Append) && savepr4.open(QIODevice::Append) &&
            savepr5.open(QIODevice::Append) && savepr6.open(QIODevice::Append))
    {
// записываем в файлы данные из переменных
       savescore.write(QByteArray::number(sco, 'f', 3));
       saveauto.write(QByteArray::number(aut, 'f', 3));
       savepr1.write(QByteArray::number(pr1, 'f', 3));
       savepr2.write(QByteArray::number(pr2, 'f', 3));
       savepr3.write(QByteArray::number(pr3, 'f', 3));
       savepr4.write(QByteArray::number(pr4, 'f', 3));
       savepr5.write(QByteArray::number(pr5, 'f', 3));
       savepr6.write(QByteArray::number(pr6, 'f', 3));
// закрываем файлы
       savescore.close();
       saveauto.close();
       savepr1.close();
       savepr2.close();
       savepr3.close();
       savepr4.close();
       savepr5.close();
       savepr6.close();
    }
}

// открываем последнее сохранение
void MainWindow::on_openSave_clicked()
{
    QFile savescore("saves/score.txt"), saveauto("saves/auto.txt"), savepr1("saves/pr1.txt")
            , savepr2("saves/pr2.txt"), savepr3("saves/pr3.txt"), savepr4("saves/pr4.txt")
            , savepr5("saves/pr5.txt"), savepr6("saves/pr6.txt");
    if(((savescore.exists())&&(savescore.open(QIODevice::ReadOnly))) &&
            ((saveauto.exists())&&(saveauto.open(QIODevice::ReadOnly))) &&
            ((savepr1.exists())&&(savepr1.open(QIODevice::ReadOnly))) &&
            ((savepr2.exists())&&(savepr2.open(QIODevice::ReadOnly))) &&
            ((savepr3.exists())&&(savepr3.open(QIODevice::ReadOnly))) &&
            ((savepr4.exists())&&(savepr4.open(QIODevice::ReadOnly))) &&
            ((savepr5.exists())&&(savepr5.open(QIODevice::ReadOnly))) &&
            ((savepr6.exists())&&(savepr6.open(QIODevice::ReadOnly))))
    {
// присваиваем значения из файлов переменным
        QString sco = savescore.readLine();
        QString aut = saveauto.readLine();
        QString pr1 = savepr1.readLine();
        QString pr2 = savepr2.readLine();
        QString pr3 = savepr3.readLine();
        QString pr4 = savepr4.readLine();
        QString pr5 = savepr5.readLine();
        QString pr6 = savepr6.readLine();

        ui->score->setText(sco);
        ui->automatic->setText(aut);
        ui->pr1->setText(pr1);
        ui->pr2->setText(pr2);
        ui->pr3->setText(pr3);
        ui->pr4->setText(pr4);
        ui->pr5->setText(pr5);
        ui->pr6->setText(pr6);

        savescore.close();
        saveauto.close();
        savepr1.close();
        savepr2.close();
        savepr3.close();
        savepr4.close();
        savepr5.close();
        savepr6.close();
    }
}
