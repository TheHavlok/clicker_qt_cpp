#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "ui_dialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_mine_clicked();
    void on_shop_clicked();
    void setauto(); // для таймера
    void on_savePush_clicked();
    void on_openSave_clicked();

private:
    Ui::MainWindow *ui;

    QTimer m_timer;
};

#endif // MAINWINDOW_H
