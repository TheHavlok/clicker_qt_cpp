#include "dialog.h"
#include "ui_dialog.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    this->setWindowFlags(Qt::Window |
                         Qt::CustomizeWindowHint |
                         Qt::WindowTitleHint |
                         Qt::Dialog |
                         Qt::MSWindowsFixedSizeDialogHint);

    // таймер для автоматическго обновления
    connect(&a_timer, SIGNAL(timeout()), SLOT(setautomatic()));
    a_timer.start(1000);

    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

// автоматическое обнавление
void Dialog::setautomatic()
{
    double autom, score;
    autom = ui->automat->text().toDouble();
    score = ui->label->text().toDouble();
    score = score + autom;
    ui->label->setText(QString::number(score, 'f', 3));

}

// принимаем значения что бы они не пропали по закрытию второго окна "Dialog"

// принимаем значение score
void Dialog::setText(const QString &text)
{
    ui->label->setText(text);
}

// принимаем значение automatic pr1
void Dialog::autom(const QString &text)
{
    ui->automat->setText(text);
}

// принимаем значение pr1
void Dialog::setPrice1(const QString &text)
{
    ui->price1->setText(text);
}

// принимаем значение pr2
void Dialog::setPrice2(const QString &text)
{
    ui->price2->setText(text);
}

// принимаем значение pr3
void Dialog::setPrice3(const QString &text)
{
    ui->price3->setText(text);
}

// принимаем значение pr4
void Dialog::setPrice4(const QString &text)
{
    ui->price4->setText(text);
}

// принимаем значение pr5
void Dialog::setPrice5(const QString &text)
{
    ui->price5->setText(text);
}

// принимаем значение pr6
void Dialog::setPrice6(const QString &text)
{
    ui->price6->setText(text);
}

void Dialog::on_buy1_clicked()
{
    double x, z, res, m, automat;
    x = ui->label->text().toDouble(); // присваиваем переменной то сколько у нас есть
    z = ui->price1->text().toDouble(); // присваиваем переменной сколько стоит первое улучшение
    if(x >= z) // сравниваем то сколько имеем и цену
    {
        res = x - z; // получаем сколько у нас останется
        m = z+(z/100*30); // на сколько будет увеличиватся цена улучшения послу покупки
        ui->price1->setText(QString::number(m, 'f', 3)); // выводим то сколько будет стоить следующее улучшение
        ui->label->setText(QString::number(res, 'f', 3)); // выводим сколько у нас останется после покупки

        automat = ui->automat->text().toDouble(); // присваиваем переменной то по сколько у нас прибавлсяется автоматически
        automat = automat + 0.001; // 0.001 то по сколько будет прибавлсятся автоматически
        ui->automat->setText(QString::number(automat, 'f', 3)); // выводим то сколько станет прибавлсятся автоматически

    // отправлем значение
        // отправлсяем то сколько мы в итоге имеем после покупки
        emit score(ui->label->text());
        // отправляем то сколько у нас будет прибавлятся автоматически
        emit automatic(ui->automat->text());
    }
}

void Dialog::on_buy2_clicked()
{
    double x, z, res, m, automat;
    x = ui->label->text().toDouble();
    z = ui->price2->text().toDouble();
    if(x >= z)
    {
        res = x - z;
        m = z+(z/100*30);
        ui->price2->setText(QString::number(m, 'f', 3));
        ui->label->setText(QString::number(res, 'f', 3));

        automat = ui->automat->text().toDouble();
        automat = automat + 0.005;
        ui->automat->setText(QString::number(automat, 'f', 3));
        emit score(ui->label->text());
        emit automatic(ui->automat->text());
    }
}

void Dialog::on_close_clicked()
{
    // отправляем стоимость что бы после закрытия окна они сохранились
    emit price1(ui->price1->text());
    emit price2(ui->price2->text());
    emit price3(ui->price3->text());
    emit price4(ui->price4->text());
    emit price5(ui->price5->text());
    emit price6(ui->price6->text());

    close();
}

void Dialog::on_buy3_clicked()
{
    double x, z, res, m, automat;
    x = ui->label->text().toDouble();
    z = ui->price3->text().toDouble();
    if(x >= z)
    {
        res = x - z;
        m = z+(z/100*30);
        ui->price3->setText(QString::number(m, 'f', 3));
        ui->label->setText(QString::number(res, 'f', 3));

        automat = ui->automat->text().toDouble();
        automat = automat + 0.025;
        ui->automat->setText(QString::number(automat, 'f', 3));

        emit score(ui->label->text());
        emit automatic(ui->automat->text());
    }
}

void Dialog::on_buy4_clicked()
{
    double x, z, res, m, automat;
    x = ui->label->text().toDouble();
    z = ui->price4->text().toDouble();
    if(x >= z)
    {
        res = x - z;
        m = z+(z/100*30);
        ui->price4->setText(QString::number(m, 'f', 3));
        ui->label->setText(QString::number(res, 'f', 3));

        automat = ui->automat->text().toDouble();
        automat = automat + 0.050;
        ui->automat->setText(QString::number(automat, 'f', 3));

        emit score(ui->label->text());
        emit automatic(ui->automat->text());
    }
}

void Dialog::on_buy5_clicked()
{
    double x, z, res, m, automat;
    x = ui->label->text().toDouble();
    z = ui->price5->text().toDouble();
    if(x >= z)
    {
        res = x - z;
        m = z+(z/100*30);
        ui->price5->setText(QString::number(m, 'f', 3));
        ui->label->setText(QString::number(res, 'f', 3));

        automat = ui->automat->text().toDouble();
        automat = automat + 0.075;
        ui->automat->setText(QString::number(automat, 'f', 3));

        emit score(ui->label->text());
        emit automatic(ui->automat->text());
    }
}

void Dialog::on_buy6_clicked()
{
    double x, z, res, m, automat;
    x = ui->label->text().toDouble();
    z = ui->price6->text().toDouble();
    if(x >= z)
    {
        res = x - z;
        m = z+(z/100*30);
        ui->price6->setText(QString::number(m, 'f', 3));
        ui->label->setText(QString::number(res, 'f', 3));

        automat = ui->automat->text().toDouble();
        automat = automat + 0.100;
        ui->automat->setText(QString::number(automat, 'f', 3));

        emit score(ui->label->text());
        emit automatic(ui->automat->text());
    }
}
